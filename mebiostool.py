#!/usr/bin/python 
import struct

fo = open("mefwrhdr.BIN", "r").read() 
ftpLoc = fo.find("$FPT")
print "Header name : %s" %(fo[ftpLoc:ftpLoc+4])
ftpEntries = fo[ftpLoc+4:ftpLoc+8]
numOfEntries = struct.unpack("I", ftpEntries)
print "Number of entries : %u" %numOfEntries
versionNumber = struct.unpack("B", fo[ftpLoc+8])
print "Version: %u" %versionNumber
entryType = struct.unpack("B", fo[ftpLoc+9])
print "Entry Type: %u" %entryType
headerLen = struct.unpack("B", (fo[ftpLoc+10]))
print "Header Length: %u" %headerLen
checkSum = struct.unpack("B", fo[ftpLoc+11])
print "Checksum: %u" %checkSum
flashCycles = struct.unpack("H", fo[ftpLoc+12:ftpLoc+14])
print "Flash cycles: %u" %flashCycles
maxFlashCycles = struct.unpack("H", fo[ftpLoc+14:ftpLoc+16])
print "Max Flash Cycles: %u" %maxFlashCycles
umaSize = struct.unpack("I", fo[ftpLoc+16:ftpLoc+20])
print "UMA Size: %u" %umaSize
flags = struct.unpack("I", fo[ftpLoc+20:ftpLoc+24])
print "Flags: %u" %flags
print ""
#print "Header %s:" %(fo[ftpLoc:ftpLoc+(3*16)])
for x in range (0, 4): 
	print "Partition name: %s" %(fo[ftpLoc+32+x*32:ftpLoc+36+x*32])
	print "Partition owner: %s" %(fo[ftpLoc+36+x*32:ftpLoc+40+x*32])
	print "Partition offset: %u" %(struct.unpack("I", fo[ftpLoc+40+x*32:ftpLoc+44+x*32]))
	print "Partition length: %u" %(struct.unpack("I", fo[ftpLoc+44+x*32:ftpLoc+48+x*32]))
	print "Partition start tokens %u" %(struct.unpack("I", fo[ftpLoc+48+x*32:ftpLoc+52+x*32]))
	print "Partition max tokens %u" %(struct.unpack("I", fo[ftpLoc+52+x*32:ftpLoc+56+x*32]))
	print "Partition scratch sectors %u" %(struct.unpack("I", fo[ftpLoc+60+x*32:ftpLoc+64+x*32]))
	print "Partition flags %u" %(struct.unpack("I", fo[ftpLoc+68+x*32:ftpLoc+72+x*32]))
	print ""

start = -1 
while True:
	start = fo.find("$MN2", start+1)
	if start == -1: break 
	print "$MN2 module location: %u" %start 
	print "Module type %u" %struct.unpack("H", fo[start-28:start-26])
	print "Module subtype %u" %struct.unpack("H", fo[start-26:start-24])
	print "Header length %u" %struct.unpack("I", fo[start-24:start-20])
	print "Header version %u" %struct.unpack("I", fo[start-20:start-16])
	print "Flags %u" %struct.unpack("I", fo[start-16:start-12])
	print "Module vendor %u" %struct.unpack("I", fo[start-12:start-8])
	print "Module date %u" %struct.unpack("I", fo[start-8:start-4])
	print "Total manifest size %u" %struct.unpack("I", fo[start-4:start])
	print "Module tag %s" %(fo[start:start+4])
	print "Number of modules : %u" %struct.unpack("I", fo[start+4:start+8])
	numOfModules = struct.unpack("I", fo[start+4:start+8])
	print "Version : %u" %struct.unpack("I", fo[start+8:start+12])
	print "Keysize : %u" %struct.unpack("<I", fo[start+92:start+96])
	print "Reserved : %u" %struct.unpack("I", fo[start+96:start+100])
	print "RSA key : %u" %struct.unpack("I", fo[start+100:start+104]),
	for y in range(1, 62):
		print "%u" %struct.unpack("I", fo[start+100+(y*4):start+104+(y*4)]),
	print "%u" %struct.unpack("I", fo[start+100+(63*4):start+104+(63*4)])
	print "RSA exponent : %u" %struct.unpack("I", fo[start+100+256:start+100+260])
	print "RSA signature : %u" %struct.unpack("I", fo[start+360:start+364]),
	for y in range(1, 62):
		print "%u" %struct.unpack("I", fo[start+360+(y*4):start+364+(y*4)]),
	print "%u" %struct.unpack("I", fo[start+360+(63*4):start+364+(63*4)])
	print ""
	print "Partition name : %s" %fo[start+616:start+628].strip()
	print ""
	for y in range(0, numOfModules[0]):
		if fo[start+632+96*y:start+648+96*y].strip().strip("\0").lower() == "pavp":
			print "\t Module : %u name : %s" %(y, fo[start+632+96*y:start+648+96*y].strip()),
			print " {Protected Audio Video Path}"
		elif fo[start+632+96*y:start+648+96*y].strip().strip("\0").lower() == "icc":
			print "\t Module : %u name : %s" %(y, fo[start+632+96*y:start+648+96*y].strip()),
			print " {Integrated Clock Control}"
		elif fo[start+632+96*y:start+648+96*y].strip().strip("\0").lower() == "cls":
			print "\t Module : %u name : %s" %(y, fo[start+632+96*y:start+648+96*y].strip()),
			print " {Capability Lisencing Service}" 
		elif fo[start+632+96*y:start+648+96*y].strip().strip("\0").lower() == "qst":
			print "\t Module : %u name : %s" %(y, fo[start+632+96*y:start+648+96*y].strip()),
			print " {Quiet System Technology}" 
		elif fo[start+632+96*y:start+648+96*y].strip().strip("\0").lower() == "tdt":
			print "\t Module : %u name : %s" %(y, fo[start+632+96*y:start+648+96*y].strip()),
			print " {Theft Deterrence Technology}" 
		elif fo[start+632+96*y:start+648+96*y].strip().strip("\0").lower() == "sessmgr":
			print "\t Module : %u name : %s" %(y, fo[start+632+96*y:start+648+96*y].strip()),
			print " {Session Manager???}" 
		elif fo[start+632+96*y:start+648+96*y].strip().strip("\0").lower() == "hostcomm":
			print "\t Module : %u name : %s" %(y, fo[start+632+96*y:start+648+96*y].strip()),
			print " {Host Communication???}" 
		else:
			print "\t Module : %u name : %s" %(y, fo[start+632+96*y:start+648+96*y].strip())
		print ""
		print "\t\t Hash : %u" %struct.unpack("I", fo[start+648+96*y:start+652+96*y]), 
		for x in range(0, 6):
			print "%u" %struct.unpack("I", fo[start+648+96*y+x*4:start+652+96*y+x*4]),
		print "%u"  %struct.unpack("I", fo[start+648+96*y+7*4:start+652+96*y+7*4])
		print "\t\t UKNOWN DATA1 %u" %struct.unpack("I", fo[start+648+96*y+3*7+4:start+652+96*y+3*7+4])
		print "\t\t UKNOWN DATA2 %u" %struct.unpack("I", fo[start+648+96*y+3*7+8:start+652+96*y+3*7+8])
		print "\t\t UKNOWN DATA3 %u" %struct.unpack("I", fo[start+648+96*y+3*7+12:start+652+96*y+3*7+12])
		print "\t\t UKNOWN DATA4 {?? FLAGS ??} %u" %struct.unpack("I", fo[start+648+96*y+3*7+16:start+652+96*y+3*7+16])
		print "\t\t UKNOWN DATA5 %u" %struct.unpack("I", fo[start+648+96*y+3*7+20:start+652+96*y+3*7+20])
		print "\t\t UKNOWN DATA6 {?? COM SIZE ??}%u" %struct.unpack("I", fo[start+648+96*y+3*7+24:start+652+96*y+3*7+24])
		print "\t\t UKNOWN DATA7 {?? UNC SIZE ??}%u" %struct.unpack("I", fo[start+648+96*y+3*7+28:start+652+96*y+3*7+28])
		print "\t\t UKNOWN DATA8 %u" %struct.unpack("I", fo[start+648+96*y+3*7+32:start+652+96*y+3*7+32])
		print "\t\t UKNOWN DATA9 %u" %struct.unpack("I", fo[start+652+96*y+3*7+32:start+656+96*y+3*7+32])
		print "\t\t UKNOWN DATA10 %u" %struct.unpack("I", fo[start+656+96*y+3*7+32:start+660+96*y+3*7+32])
		print "\t\t UKNOWN DATA11 %u" %struct.unpack("I", fo[start+664+96*y+3*7+32:start+668+96*y+3*7+32])


		print "" 
	print "" 
#start = -1
#while True:
#	start = fo.find("$MME",	start+1)
#	if start == -1: break
#	print "$MME modue location: %u" %start 
#	print "Header tag %s" %fo[start+4:start+9].strip()
#	print "Module Hash"
#	for x in range(0, 31): 
#		print("%x") %ord(fo[start+20+x]),
#	print("%x") %ord(fo[start+20+31])


