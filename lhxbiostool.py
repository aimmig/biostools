#!/usr/bin/python 
import struct
import os
import os.path
import shutil
import sys
from itertools import chain

fo = open(sys.argv[1], "r").read() 
lh5loc = -1
numLH5 = 0;
os.mkdir("LH5")
prevDir = os.getcwd()
os.chdir(os.getcwd()+"/LH5/")
os.mkdir("exec")
while True:
	lh5loc = fo.find("-lh5-", lh5loc+1)
	if lh5loc == -1: break
	#print "size of base header: 0x%x" %struct.unpack("B", fo[lh5loc-2])
	headerSize = struct.unpack("B", fo[lh5loc-2])[0]	
	#print "base header checksum: 0x%x" %struct.unpack("B", fo[lh5loc-1])
	#print "compression method: %s" %fo[lh5loc:lh5loc+5]
	#print "compressed file size: 0x%x" %struct.unpack("<I", fo[lh5loc+5:lh5loc+9])
	totalFileSize = struct.unpack("<I", fo[lh5loc+5:lh5loc+9])[0]
	#print "uncompressed file size: 0x%x" %struct.unpack("<I", fo[lh5loc+9:lh5loc+13])
	#print "decompression offset address: 0x%x" %struct.unpack("H", fo[lh5loc+13:lh5loc+15])
	#print "decompression segment address: 0x%x" %struct.unpack("H", fo[lh5loc+15:lh5loc+17])
	#print "file attribute: 0x%x" %struct.unpack("B", fo[lh5loc+17])
	#print "header level: 0x%x" %struct.unpack("B", fo[lh5loc+18])
	#print "name legnth: 0x%x" %struct.unpack("B", fo[lh5loc+19])
	nameLen = struct.unpack("B", fo[lh5loc+19])[0]
	#print "name: %s" %fo[lh5loc+20:lh5loc+20+nameLen]
	name = fo[lh5loc+20:lh5loc+20+nameLen]
	#print "component CRC16: 0x%x" %struct.unpack("H", fo[lh5loc+20+nameLen:lh5loc+20+nameLen+2])
	#print "operating system ID: 0x%x" %struct.unpack("B", fo[lh5loc+20+nameLen+2])
	#print "next header size: 0x%x" %struct.unpack("B", fo[lh5loc+20+nameLen+3])
	#print ""
	#print ""
	lh5file = fo[lh5loc-2:lh5loc+totalFileSize+headerSize]
	#print "first byte in file %u" %struct.unpack("B", lh5file[0])
	#print "last byte in file %u"  %struct.unpack("B", lh5file[totalFileSize+headerSize+1])
	fwrite = open(name+"comp", 'wb')
	fwrite.write(lh5file)
	fwrite.close()
	osString = "7z e "
	osString += (name+"comp ")
	osString += ("> /dev/null")
	#print "%s" %osString
	os.system(osString)
	os.remove(name+"comp")
	#decomp = open(name, "r").read()
	#print "10 bytes: %s" %decomp[0:10]
	numLH5+=1
	decomp = open(name, "rb").read()
	
	numByte = 0;
	byteLoc = -1
	#while True: 
		#byteLoc = decomp.find('\x0f\xaa', byteLoc+1)
		#if byteLoc == -1: break
		#numByte+=1

	#print "number of times 0faa occurs %u in %s" %(numByte, name)

	#print "numbers %s %u %s" %(decomp[0],struct.unpack("B", decomp[1])[0], name)
	if decomp[0:2] == "MZ" or struct.unpack("B", decomp[0])[0] == 0x55 and struct.unpack("B", decomp[1])[0] == 0xAA:
		shutil.copyfile(os.getcwd()+"/"+name, os.getcwd()+"/exec/"+name)
		os.remove(name)

print "number of LH5 modules extracted %u" %numLH5

os.chdir(prevDir)
os.mkdir("LH0")
os.chdir(prevDir+"/LH0/")
os.mkdir("exec")
numLH0 = 0
lh0loc = -1
while True:
	lh0loc = fo.find("-lh0-", lh0loc+1)
	if lh0loc == -1: break
	#print "size of base header: 0x%x" %struct.unpack("B", fo[lh5loc-2])
	headerSize = struct.unpack("B", fo[lh0loc-2])[0]	
	#print "base header checksum: 0x%x" %struct.unpack("B", fo[lh5loc-1])
	#print "compression method: %s" %fo[lh5loc:lh5loc+5]
	#print "compressed file size: 0x%x" %struct.unpack("<I", fo[lh5loc+5:lh5loc+9])
	totalFileSize = struct.unpack("<I", fo[lh0loc+5:lh0loc+9])[0]
	#print "uncompressed file size: 0x%x" %struct.unpack("<I", fo[lh5loc+9:lh5loc+13])
	#print "decompression offset address: 0x%x" %struct.unpack("H", fo[lh5loc+13:lh5loc+15])
	#print "decompression segment address: 0x%x" %struct.unpack("H", fo[lh5loc+15:lh5loc+17])
	#print "file attribute: 0x%x" %struct.unpack("B", fo[lh5loc+17])
	#print "header level: 0x%x" %struct.unpack("B", fo[lh5loc+18])
	#print "name legnth: 0x%x" %struct.unpack("B", fo[lh5loc+19])
	nameLen = struct.unpack("B", fo[lh0loc+19])[0]
	#print "name: %s" %fo[lh5loc+20:lh5loc+20+nameLen]
	name = fo[lh0loc+20:lh0loc+20+nameLen]
	#print "component CRC16: 0x%x" %struct.unpack("H", fo[lh5loc+20+nameLen:lh5loc+20+nameLen+2])
	#print "operating system ID: 0x%x" %struct.unpack("B", fo[lh5loc+20+nameLen+2])
	#print "next header size: 0x%x" %struct.unpack("B", fo[lh5loc+20+nameLen+3])
	#print ""
	#print ""
	lh5file = fo[lh0loc-2:lh0loc+totalFileSize+headerSize]
	#print "first byte in file %u" %struct.unpack("B", lh5file[0])
	#print "last byte in file %u"  %struct.unpack("B", lh5file[totalFileSize+headerSize+1])
	fwrite = open(name+"comp", 'wb')
	fwrite.write(lh5file)
	fwrite.close()
	osString = "7z e "
	osString += (name+"comp ")
	osString += ("> /dev/null")
	#print "%s" %osString
	os.system(osString)
	os.remove(name+"comp")
	#decomp = open(name, "r").read()
	#print "10 bytes: %s" %decomp[0:10]
	numLH0+=1
	decomp = open(name, "rb").read()

	numByte = 0;
	byteLoc = -1
	#while True: 
		#byteLoc = decomp.find('\x0f\xaa', byteLoc+1)
		#if byteLoc == -1: break
		#numByte+=1

	#print "number of times 0faa occurs %u in %s" %(numByte, name)

	if decomp[0:2] == "MZ" or struct.unpack("B", decomp[0])[0] == 0x55 and struct.unpack("B", decomp[1])[0] == 0xAA:
		shutil.copyfile(os.getcwd()+"/"+name, os.getcwd()+"/exec/"+name)
		os.remove(name)

print "number of LH0 modules extracted %u" %numLH0 
os.chdir(prevDir)

dollarStart = -1
symbols = []
while True:
	dollarStart = fo.find("$MME", dollarStart+1)
	if dollarStart == -1: break 
	print "$MME found %s" %(fo[dollarStart:fo.find('\x00', dollarStart)]) 
	symbols.append(fo[dollarStart:fo.find('\x00', dollarStart)])

paths = ('/LH0/', '/LH5/')

for dirpath, dirnames, filenames in chain.from_iterable(os.walk(os.getcwd()+path) for path in paths):
    for filename in [f for f in filenames]:
        lfo = open(os.path.join(dirpath, filename), "r").read()
        if any(symbol in lfo for symbol in symbols):
        	print "symbols found in file %s" %(os.path.join(dirpath, filename)) 
        	for sym in symbols:
        		if lfo.find(sym) != -1:
        			start = lfo.find(sym)
        			print "symbol %s at %d" %(lfo[start:lfo.find('\x00', start)], lfo.find(lfo[start:lfo.find('\x00', start)]))







